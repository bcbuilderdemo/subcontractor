import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { NavbarComponent } from './templates/navbar/navbar.component';
import { SidebarComponent } from './templates/Sidebar/sidebar.component';
import { topbarComponent } from './templates/topbar/topbar.component';
import { DataService } from './templates/topbar/topbar.component';

import { HomePageComponent } from './pages/home-page/home-page.component';
import { dashboardComponent } from './pages/dashboard/dashboard.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { NewProjectComponent } from './pages/project-pages/new-project/new-project.component';
import { ProjectDetailsComponent } from './pages/project-pages/project-details/project-details.component';
import { ContractorsComponent } from './pages/services/contractors/contractors.component';
import { ViewComponent } from './pages/services/view/view.component';
import { RegisterComponent } from './pages/signup-login/register/register.component';
import { LoginComponent } from './pages/signup-login/login/login.component';
import { NewBidComponent } from './pages/bids/new-bid/new-bid.component';
import { RequestBidFromSubcontractorComponent } from './pages/bids/request-bid-from-subcontractor/request-bid-from-subcontractor.component';
import {
  AccordionModule, AlertModule, CarouselModule, ModalModule, PaginationModule,
  TooltipModule
} from 'ngx-bootstrap';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BackendService } from './services/backend.service';
import { AlertsComponent } from './templates/alerts/alerts.component';
import { AlertService } from './services/alerts/alert.service';
import { ModalComponent } from './templates/modal/modal.component';
import { SendBidComponent } from './pages/services/send-bid/send-bid.component';
import { Ng2Webstorage } from 'ngx-webstorage';
import { AuthService } from './services/auth/auth.service';
import { AuthModule } from './services/auth/auth.module';
import { ForgotPasswordComponent } from './pages/password/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/password/reset-password/reset-password.component';
import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { TextMaskModule } from 'angular2-text-mask';
import { GlobalConstants } from './constants/global.constants';
import { ViewBidsComponent } from './pages/bids/view-bids/view-bids.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ProjectRouteGuard } from './services/route-guard/route-guard.service';
import { ProjectResolverService } from './services/resolvers/project-resolver/project-resolver.service';
import { NewOrCreateBidResolverService } from './services/resolvers/new-or-create-bid/new-or-create-bid.service';
import { GetSCByTradeResolver } from './services/resolvers/get-scby-trade/get-sc-by-trade.resolver.service';
import { APP_BASE_HREF, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { GetSctsOrProjectsOrBidTypeResolverService } from './services/resolvers/get-scts-or-projects/get-scts-or-projects-resolver.service';
import { GetBidsResolverService } from './services/resolvers/get-bids/get-bids-resolver.service';
import { GetPermissionsService } from './services/resolvers/get-permissions/get-permissions.service';
import { AllBidsComponent } from './pages/bids/all-bids/all-bids.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { BidFormComponent, ModalContentComponent } from './pages/bids/bid-form/bid-form.component';
import { GetScboDataService } from './services/resolvers/get-scbo-data/get-scbo-data.service';
import { CanViewOrManageBidGuard } from './services/guards/can-view-or-manage-bid/can-view-or-manage-bid.guard.guard';
import { MomentModule } from 'angular2-moment';
import { ViewUploadsComponent } from './pages/uploads/view-uploads/view-uploads.component';
import { GetUploadsForProjectService } from './services/resolvers/get-uploads-for-project/get-uploads-for-project.service';
import { ChangelogComponent } from './pages/temporary/changelog/changelog.component';
import { GetFilesForProjectOrBidService } from './services/resolvers/get-files-for-project-or-bid/get-files-for-project-or-bid.service';
import { GetBidForProjectForScResolverService } from './services/resolvers/get-bid-for-project-for-sc-resolver/get-bid-for-project-for-sc-resolver.service';
import { ViewAccountingComponent } from './pages/accounting/view-accounting/view-accounting.component';
import { GetAccountingDataResolverService } from './services/resolvers/get-accounting-data-resolver/get-accounting-data-resolver.service';
import { InvoiceComponent } from './pages/accounting/invoice/invoice.component';
import { GetInvoiceDataResolverService } from './services/resolvers/get-invoice-data-resolver/get-invoice-data-resolver.service';
import { ViewBidComponent } from './pages/bids/view-bid/view-bid.component';
import { ReviewBidComponent } from './pages/bids/review-bid/review-bid.component';
import { GetBuilderBidOutDataResolverService } from './services/resolvers/get-builder-bid-out-data-resolver/get-builder-bid-out-data-resolver.service';
import { GetCreateOrEditBbodetailsResolverService } from './services/resolvers/get-create-or-edit-bbodetails-resolver/get-create-or-edit-bbodetails-resolver.service';
import { GetBidsLineItemsResolverService } from './services/resolvers/get-bids-line-items/get-bids-line-items-resolver.service'
import { CheckIfBboExistsResolverService } from './services/resolvers/check-if-bbo-exists/check-if-bbo-exists-resolver.service';
import { ProfileComponent } from './pages/profile/profile/profile.component';
import { ProfileResolverService } from './services/resolvers/profile-resolver/profile-resolver.service';
import { ViewProfileComponent } from './pages/profile/view-profile/view-profile.component';
import { ChatComponent } from './pages/chat/chat/chat.component';
import { ChatResolverService } from './services/resolvers/chat-resolver/chat-resolver.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { Autosize } from 'ng-autosize';
import { GetDashboardDataResolverServiceService } from './services/resolvers/get-dashboard-resolver-service/get-dashboard-data-resolver-service.service';
import { GetProjectDataResolverService } from './services/resolvers/get-project-resolver-service/get-project-data-resolver-service.service';
import { GetAllNotificationsResolverService } from './services/resolvers/get-all-notifications/get-all-notifications-resolver.service';
import { ViewMessagesComponent } from './pages/chat/view-messages/view-messages.component';
import { InvoiceSendComponent } from './pages/bids/invoice/invoice-send.component';
import { ViewAllMessagesResolverService } from './services/resolvers/view-all-messages-resolver/view-all-messages-resolver.service';
import { KeyObjectArrayPipePipe } from './pipes/key-object-array-pipe/key-object-array.pipe.pipe';
import { GetAllSubcontractorsResolverService } from './services/resolvers/get-all-scsresolver/get-all-subcontractors-resolver.service';
import { GetHomeBuildersGuideResolverService } from './services/resolvers/get-hbgresolver/get-home-builders-guide-resolver.service';
import { GetProjectsWithSubProjectsDataResolverService } from './services/resolvers/get-projects-with-sub-projects/get-projects-with-sub-projects.service';
import { LineNavbarComponent } from './templates/line-navbar/line-navbar.component';
import { SocialProfileComponent } from './pages/social/social.component';
import { SocialProfilePhotoComponent } from './pages/social/photo.component';
import { SubprojectResolverService } from './services/resolvers/subproject-resolver/subproject-resolver.service';
//Layout Modules
import { CommonLayoutComponent } from './common/common-layout.component';
import { AuthenticationLayoutComponent } from './common/authentication-layout.component';

//Directives
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PopoverModule } from 'ngx-bootstrap';
import { Sidebar_Directives } from './shared/directives/side-nav.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { NguiMapModule } from '@ngui/map';
import { GoogleMapComponent } from './pages/maps/google-map/google-map.component';

export function authServiceFactory(auth: AuthService): Function {
  return () => auth.onStartupRefresh();
}
const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: HomePageComponent,
    resolve: {
      permissions: GetPermissionsService,
      data: GetDashboardDataResolverServiceService,
      nots: GetAllNotificationsResolverService,
      profile: ProfileResolverService,
      SCOrProjectData: GetSctsOrProjectsOrBidTypeResolverService,
      bidData: GetBidsResolverService,
    }
  },
  {
    path: 'maps/google-map',
    component: GoogleMapComponent,
    resolve: {
      permissions: GetPermissionsService,
      data: GetProjectDataResolverService,
      // allData: GetProjectsWithSubProjectsDataResolverService,
      nots: GetAllNotificationsResolverService,
      profile: ProfileResolverService,
    }
  },
  {
    path: 'changelog',
    component: ChangelogComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'profile',
    component: SocialProfileComponent,
    resolve: { data: ProfileResolverService }
  },
  {
    path: 'profile-photos',
    component: SocialProfilePhotoComponent,
    resolve: { data: ProfileResolverService }
  },
  {
    path: 'editprofile',
    component: ProfileComponent,
    resolve: { data: ProfileResolverService },
  },

  {
    path: 'profile/:companySlug',
    component: ViewProfileComponent,
    resolve: { data: ProfileResolverService },
  },
  {
    path: 'messages',
    component: ViewMessagesComponent,
    resolve: { data: ViewAllMessagesResolverService }
  },
  /*{
    path: 'subcontractors/:scTradeID/profile/:scID',
    component: ViewProfileComponent,
  },*/
  {
    path: 'invoice/project_id/:project_id/subproject_id/:subproject_id',
    component: InvoiceSendComponent,
    resolve: {
      bidInfo: GetBidsLineItemsResolverService,
      profile: ProfileResolverService,
      projectData: ProjectResolverService,
    }
  },
  {
    path: 'bids/:type',
    component: ViewBidsComponent,
    resolve: {
      SCOrProjectData: GetSctsOrProjectsOrBidTypeResolverService,
      bidData: GetBidsResolverService,
      permissions: GetPermissionsService,
    }
  },
  {
    path: 'accounting/:type',
    component: ViewAccountingComponent,
    resolve: {
      data: GetAccountingDataResolverService
    }
  },
  {
    path: 'projects/:id/bids/:bidID/:uType/quote',
    component: BidFormComponent,
    canActivate: [CanViewOrManageBidGuard],
    resolve: {
      data: GetScboDataService
    }
  },
  {
    path: 'projects/:id',
    // canActivate: [ProjectRouteGuard],
    children:
      [
        {
          path: 'new-project',
          component: NewProjectComponent,
        },
        {
          path: 'project-details',
          component: ProjectDetailsComponent,
          resolve: {
            projectData: ProjectResolverService,
            nots: GetAllNotificationsResolverService,
            profile: ProfileResolverService,
          }
        },
        /***********************************************
                        SUBCONTRACTORS
         ************************************************/
        {
          path: 'subcontractors',
          component: ContractorsComponent,
          resolve: {
            scs: GetAllSubcontractorsResolverService,
            hbg: GetHomeBuildersGuideResolverService,
            subprojects: SubprojectResolverService,
          }
        },
        {
          path: 'subcontractors/view/:scTradeID/:hgbID',
          component: ViewComponent,
          resolve: {
            data: GetSCByTradeResolver,
            //bidExists: CheckIfBboExistsResolverService
            subprojects: SubprojectResolverService,
          },
        },
        {
          path: ':subproject_id/invoice',
          component: InvoiceSendComponent,
          resolve: {
            bidInfo: GetBidsLineItemsResolverService,
            profile: ProfileResolverService,
            projectData: ProjectResolverService,
          }
        },
        /***********************************************
                          BIDS
         ************************************************/
        {
          path: 'bids/:type',
          component: ViewBidsComponent,
          resolve: {
            SCOrProjectData: GetSctsOrProjectsOrBidTypeResolverService,
            bidData: GetBidsResolverService,
            permissions: GetPermissionsService,
          }
        },

        {
          path: 'bids',
          component: ViewBidsComponent,
          resolve: {
            // permissions:      GetPermissionsService,
            SCOrProjectData: GetSctsOrProjectsOrBidTypeResolverService,
            bidData: GetBidsResolverService,
          },
        },
        {
          path: 'bid/:type/:sctID',
          component: ViewBidComponent,
          resolve: {
            data: GetBuilderBidOutDataResolverService,
          }
        },
        {
          path: 'bids/:bidID/:uType/quote',
          component: BidFormComponent,
          canActivate: [CanViewOrManageBidGuard],
          resolve: {
            data: GetScboDataService
          }
        },
        {
          path: 'bids/:bidID/:type/:bboID',
          component: ViewBidComponent,
          resolve: {
            data: GetBuilderBidOutDataResolverService,

          },
        },
        {
          path: 'bid/:bidId/subproject/:subproject_id',
          component: ReviewBidComponent,
          resolve: {
            bidInfo: GetBidsLineItemsResolverService,
            profile: ProfileResolverService,
            projectData: ProjectResolverService,
          }
        },
        /***********************************************
                              UPLOADS
         ************************************************/
        {
          path: 'uploads',
          component: ViewUploadsComponent,
          resolve: {
            data: GetFilesForProjectOrBidService
          },
        },
        {
          path: 'uploads/:bidID',
          component: ViewUploadsComponent,
          resolve: {
            data: GetFilesForProjectOrBidService
          }
        },
        /***********************************************
                            ACCOUNTING
         ************************************************/
        {
          path: 'accounting/:type',
          component: ViewAccountingComponent,
          resolve: {
            data: GetAccountingDataResolverService
          }
        },
        {
          path: 'accounting/:accountingID/invoice',
          component: InvoiceComponent,
          resolve: {
            data: GetInvoiceDataResolverService
          }
        },
        /***********************************************
                            CHAT
         ************************************************/
        {
          path: 'chat/:uuid',
          component: ChatComponent,
          resolve: { data: ChatResolverService }
        },
        {
          path: 'messages',
          component: ViewMessagesComponent,
          resolve: { data: ViewAllMessagesResolverService }
        },
      ]
  },
  /*      {
		  path: 'services',
		  children: [
			{
			  path: 'subcontractors',
			  component: ContractorsComponent,
			  children: [
				{ path: '', redirectTo: 'view', pathMatch: 'full' },
				{path: 'view', component: ViewComponent},
			  ]
			},

		  ]}*/

  {
    path: 'password/forgot',
    component: ForgotPasswordComponent,
  },
  {
    path: 'password/reset',
    component: ResetPasswordComponent
  }];

@NgModule({
  imports: [
    BrowserModule,
    AuthModule,
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyAQADTYmLQopZpAs_tb5iwz7XgNgOK1urg' }),
    HttpModule,
    FormsModule,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    Ng2Webstorage,
    TextMaskModule,
    MomentModule,
    PopoverModule.forRoot(),
    SweetAlert2Module.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    PaginationModule.forRoot(),
    AccordionModule.forRoot(),
    RouterModule.forRoot(
      routes,
      {
        // Tell the router to use the HashLocationStrategy.
        useHash: true
      },
    ),
    NgxPermissionsModule.forRoot(),
    CarouselModule.forRoot(),
    InfiniteScrollModule
  ],
  declarations: [
    AppComponent,
    NavbarComponent,
    Sidebar_Directives,
    SidebarComponent,
    topbarComponent,
    CommonLayoutComponent,
    HomePageComponent,
    dashboardComponent,
    NewProjectComponent,
    ProjectDetailsComponent,
    ContractorsComponent,
    ViewComponent,
    RegisterComponent,
    ReviewBidComponent,
    LoginComponent,
    NewBidComponent,
    InvoiceSendComponent,
    RequestBidFromSubcontractorComponent,
    AlertsComponent,
    ModalComponent,
    SendBidComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ViewBidsComponent,
    AllBidsComponent,
    BidFormComponent,
    ViewUploadsComponent,
    ChangelogComponent,
    ViewAccountingComponent,
    InvoiceComponent,
    ViewBidComponent,
    ProfileComponent,
    SocialProfileComponent,
    SocialProfilePhotoComponent,
    ViewProfileComponent,
    ChatComponent,
    Autosize,
    ViewMessagesComponent,
    KeyObjectArrayPipePipe,
    LineNavbarComponent,
    GoogleMapComponent,

  ],

  providers: [
    { provide: LOCALE_ID, useValue: 'en-CA' },
    BackendService,
    GlobalConstants,
    AlertService,
    {
      // Provider for APP_INITIALIZER
      provide: APP_INITIALIZER,
      useFactory: authServiceFactory,
      deps: [AuthService],
      multi: true
    },
    AuthService,
    ProjectRouteGuard,
    ProjectResolverService,
    NewOrCreateBidResolverService,
    GetBidsLineItemsResolverService,
    GetSCByTradeResolver,
    GetSctsOrProjectsOrBidTypeResolverService,
    GetBidsResolverService,
    GetPermissionsService,
    GetScboDataService,
    CanViewOrManageBidGuard,
    GetUploadsForProjectService,
    GetFilesForProjectOrBidService,
    GetAccountingDataResolverService,
    GetInvoiceDataResolverService,
    GetBuilderBidOutDataResolverService,
    CheckIfBboExistsResolverService,
    GetCreateOrEditBbodetailsResolverService,
    GetProjectsWithSubProjectsDataResolverService,
    ProfileResolverService,
    ChatResolverService,
    GetDashboardDataResolverServiceService,
    GetProjectDataResolverService,
    GetAllNotificationsResolverService,
    ViewAllMessagesResolverService,
    GetAllSubcontractorsResolverService,
    GetHomeBuildersGuideResolverService,
    DataService,
    SubprojectResolverService,
    // {provide: APP_BASE_HREF, useValue: '/test'}
    NguiMapModule,
  ],
  entryComponents: [
    // NewBidComponent,
    RequestBidFromSubcontractorComponent
  ],
  bootstrap
    :
    [AppComponent]
})

export class AppModule {
}
