export interface GetBidsByTradeInterface {

  bidsIn:                 BidsInInterface;
  bidsOut:                BidsInInterface;
  acceptedBids:           BidsInInterface | BidsOutInterface;
}
export interface BidsInInterface {

  id:                     number;
  projectID:              number;
  builderID:              number;
  scTradeID:              number;
  additionalDetails:      string;
  created_at:             string;
  updated_at:             string;
  b_created_at:           string;
  b_updated_at:           string;
  sc_created_at:          string;
  scboli_created_at:      string;
  scboli_updated_at:      string;
  company:                string;
  address:                string;
  address2:               string;
  phoneNumber:            string;
  postalCode:             string;
  city:                   string;
  scViewed:               number;
  scViewedAt:             string;
  scResponded?:           number;
  scRespondedAt:          string;
  builderAccepted?:       number;
  builder_accepted_at?:   number;
  bbo_accepted?:          number;
  scbo_updated?:          number;
  builder_rejected_at?:   string;
  bboID?:                 number;
  bidAcceptedSCBOID?:     number;
  scboID?:                number;
  accountingID?:          number;
  province?:              string;
  companySlug?:           string;
  chatUUID?:              string;
  bidViewedByBuilder?:    string;
  bidViewedBySC?:         string;
  builderAcceptedAtSCBO?: string;
  builderRejectedAtSCBO?: string;
  scbo_created_at?:       string;
  scbo_updated_at?:       string;
  logoURL:                string;

  bci_logoURL?:           string;
  sci_logoURL?:           string;

  bbo_scViewed:          number;
  bbo_scViewedAt:       string;
}

export interface BidsOutInterface {

  id:                     number;
  projectID:              number;
  builderID:              number;
  scTradeID:              number;
  additionalDetails:      string;
  created_at:             string;
  updated_at:             string;
  b_created_at:           string;
  b_updated_at:           string;
  sc_created_at:          string;
  scboli_created_at:      string;
  scboli_updated_at:      string;
  company:                string;
  address:                string;
  address2:               string;
  phoneNumber:            string;
  postalCode:             string;
  city:                   string;
  scViewed:               number;
  scViewedAt:             string;
  scResponded:            number;
  scRespondedAt:          string;
  builderAccepted?:       number;
  builder_accepted_at?:   number;
  bbo_accepted?:          number;
  scbo_updated?:          number;
  builder_rejected_at?:   string;
  bboID?:                 number;
  bidAcceptedSCBOID?:     number;
  scboID?:                number;
  accountingID?:          number;
  province?:              string;
  companySlug?:           string;
  chatUUID?:              string;
  bidViewedByBuilder?:    string;
  bidViewedBySC?:         string;

  bci_logoURL?:           string;
  sci_logoURL?:           string;
}

