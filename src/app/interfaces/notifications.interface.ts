export interface GetNotiticationsInterface {

  uuid:           string;
  data:           Array<NotificationsInterface>;
}


export interface NotificationsInterface {

  uuid:           string;
}
