export interface GetHBGInterface {

  stepNum:          number;
  stepName:         string;
  stepDescription:  string;
  sctID:            number;
}
