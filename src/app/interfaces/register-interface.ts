export interface RegisterInterface {

  userType:               string;
  trade?:                 string;
  email:                  string;
  password:               string;
  confirmation_password:  string;

  company:                string;
  address:                string;
  address2?:              string;
  city:                   string;
  province:               string;
  postalCode:             string;
  phoneNumber:            string;

  contactFirstName:       string;
  contactLastName:        string;
  phoneNumber2:           string;

}
