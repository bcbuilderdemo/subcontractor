import {ProfileUploadsInterface} from './profile-info.interface';

export interface GetChatAndUploadsInterface {

  chat:         ChatPaginatorInterface;
  uploads:       ProfileUploadsInterface;

}

export interface ChatPaginatorInterface {

  to:                   number;
  from:                 number;
  page:                 number;
  total:                number;
  current_page:         number;
  last_page:            number;
  per_page:             number;

  next_page_url:        string;
  prev_page_url:        string;
  path:                 string;

  data:                 Array<ChatMessagesInterface>;

}

export interface ChatMessagesInterface {

  chatUUID:                 string;
  projectID:                number;
  scID:                     number;
  builderID:                number;
  currentUserID:            number;

  id:                       number;
  message:                  string;
  sentByID:                 number;
  created_at:               string;
  updated_at:               string;
  read:                     number;

  projectAddress:           string;
  projectAddress2:          string;
  projectCity:              string;
  projectPostalCode:        string;
  projectProvince:          string;

  bci_company:             string;
  bci_companySlug:         string;
  bci_logoURL:             string;

  sci_company:            string;
  sci_companySlug:        string;
  sci_logoURL:            string;

  currentUserLogoURL:     string;
  otherUserLogoURL:       string;
}
