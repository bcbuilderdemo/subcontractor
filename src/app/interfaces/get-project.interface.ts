import {UserAndCompanyInfo} from './user-and-company-info';

export interface GetUserInfoAndProjects {

  projectInfo:      GetProjectsInterface;
  userInfo:         UserAndCompanyInfo;
}

export interface GetProjectsInterface {

  to:                   number;
  from:                 number;
  page:                 number;
  total:                number;
  current_page:         number;
  last_page:            number;
  per_page:             number;
  next_page_url:        string;
  prev_page_url:        string;
  path:                 string;
  data:                 Array<ProjectDataInterface>;

}

export interface GetProjectsWithSubProjectsInterface {
  id:             string;
  address:        string;
  address2:       string;
  buildValue:     string;
  buildingSize:   string;
  city:           string;
  country:        string;
  created_at:     string;
  description:    string;
  details:        string;
  landType:       string;
  firstName:      string;
  lastName:       string;
  lotSize:        string;
  postalCode:     string;
  projectType:    string;
  province:       string;
  updated_at:     string;
  userID:         string;
  zoning:         string;
  phoneNumber:    string;
  phoneNumber2:   string;
  numUnits:       string;

  subprojects:    Array<GetSubProjectsInterface>;
}

export interface GetSubProjectsInterface {
  project_id:           number;
  hbg_id:               number;
  is_filled:            number;
  is_available:         number;
  comments:             string;
  description:          string;
}

export interface  ProjectDataInterface {
  id:             string;
  address:        string;
  address2:       string;
  buildValue:     string;
  buildingSize:   string;
  city:           string;
  country:        string;
  created_at:     string;
  description:    string;
  details:        string;
  landType:       string;
  firstName:      string;
  lastName:       string;
  lotSize:        string;
  postalCode:     string;
  projectType:    string;
  province:       string;
  updated_at:     string;
  userID:         string;
  zoning:         string;
  phoneNumber:    string;
  phoneNumber2:   string;
  numUnits:       string;

  bidID?:           number;
  bboID?:           number;
  accountingID?:    number;

  bidAdditionalDetails?: string;
}
