export interface Alert {
  type:             AlertType;
  message:          string;
  messageType:      string;
}

export enum AlertType {
  Success,
  Error,
  Info,
  Warning
}
