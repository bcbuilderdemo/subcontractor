export interface InvoiceInterface {

  id:                     number;
  accountingID:           number;
  amountPaid:             number;
  scConfirmedPayment:     number;
  scConfirmedPaymentOn:   string;
  paymentMadeOn:          string;
  created_at:             string;
  updated_at:             string;
  scUnConfirmedPaymentOn: string;

}

export interface AccountingPaymentsMade {

  id:                     number;
  accountingID:           number;
  amountPaid:             number;
  scConfirmedPayment:     number;
  scConfirmedPaymentOn:   string;
  paymentMadeOn:          string;
  created_at:             string;
  updated_at:             string;
  scUnConfirmedPaymentOn: string;

}
