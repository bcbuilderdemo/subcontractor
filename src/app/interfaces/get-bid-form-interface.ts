export interface GetBidFormInterface {

  bidForm:            BidFormInterface;
  files:              FilesInterface;

}

export interface BidFormInterface {
  id:                 number;
  projectID:          number;
  builderID:          number;
  scTradeID:          number;
  additionalDetails:  string;
}

export interface FilesInterface {
  id:                 number;
  fileName:           string;
  mimeType:           string;
}

