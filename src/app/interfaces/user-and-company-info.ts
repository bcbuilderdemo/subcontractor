export interface UserAndCompanyInfo {

  contactFirstName:     string;
  contactLastName:      string;
  address:              string;
  address2:             string;
  postalCode:           string;
  province:             string;
  city?:                string;
  country:              string;
  email:                string;
  phoneNumber:          string;
  phoneNumber2:         string;
  company:              string;
  companySlug?:         string;
  uuid?:                string;
  logoURL?:             string;

}
