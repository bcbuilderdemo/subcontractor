export interface ProjectInfoAliasInterface {

  projectAddress:               string;
  projectAddress2:              string;
  projectCity:                  string;
  projectPostalCode:            string;
  projectProvince:              string;
  projectBuildValue:            string;
  projectBuildingSize:          string;
  projectCountry:               string;
  projectCreated_at:            string;
  projectDescription:           string;
  projectDetails:               string;
  projectLandType:              string;
  projectFirstName:             string;
  projectLastName:              string;
  projectLotSize:               string;
  projectProjectType:           string;
  projectUpdated_at:            string;
  projectZoning:                string;
  projectPhoneNumber:           string;
  projectPhoneNumber2:          string;
  projectNumUnits:              string;
}

export interface BuilderInfoAliasInterface {
  bci_company:              string;
  bci_address:              string;
  bci_address2:             string;
  bci_phoneNumber:          string;
  bci_postalCode:           string;
  bci_city:                 string;
  bci_province:             string;
}

export interface SCInfoAliasInterface {
  sci_company:              string;
  sci_address:              string;
  sci_address2:             string;
  sci_phoneNumber:          string;
  sci_postalCode:           string;
  sci_city:                 string;
  sci_province:             string;
}
