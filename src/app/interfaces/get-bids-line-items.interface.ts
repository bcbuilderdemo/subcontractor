export interface GetBidsLineItemsInterface {
  bids_id;
  details;
  quantity;
  price;
}
