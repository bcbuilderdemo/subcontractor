import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keyObjectArrayPipe'
})
export class KeyObjectArrayPipePipe implements PipeTransform {
  transform(value: any): {address: string, data: any[]}[]
  {
    const result = [];

    Object.keys(value).map((key) =>
    {
      const group = {address: key, data: []};

      Object.keys(value[key]).map((key2) =>
      {
        group.data.push(value[key][key2]);
      });

      result.push(group);
    });

    return result;
  }
}
