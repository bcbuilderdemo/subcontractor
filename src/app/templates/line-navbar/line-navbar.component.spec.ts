import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineNavbarComponent } from './line-navbar.component';

describe('LineNavbarComponent', () => {
  let component: LineNavbarComponent;
  let fixture: ComponentFixture<LineNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
