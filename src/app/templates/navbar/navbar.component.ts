import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn;
  showlineNav       = true;
  public app : any;
  public headerThemes: any;
  public changeHeader: any;
  public sidenavThemes: any;
  public changeSidenav: any;
  public headerSelected: any;
  public sidenavSelected : any;

  constructor
  (
    private auth: AuthService,
    private router: Router,
  )
  {
    this.app = {
      layout: {
          sidePanelOpen: false,
          isMenuOpened: true,
          isMenuCollapsed: false,
          themeConfigOpen: false,
          rtlActived: false
      }
  };  

  this.headerThemes = ['header-default', 'header-primary', 'header-info', 'header-success', 'header-danger', 'header-dark'];
  this.changeHeader = changeHeader;

  function changeHeader(headerTheme) {
      this.headerSelected = headerTheme;
  }

  this.sidenavThemes = ['sidenav-default', 'side-nav-dark'];
  this.changeSidenav = changeSidenav;

  function changeSidenav(sidenavTheme) {
      this.sidenavSelected = sidenavTheme;
  }
  
  }

  ngOnInit() {

    this.auth.authenticationNotifier().subscribe(authd => {
      this.isLoggedIn = authd;
    });

    /*this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd ) {
        const path      = event.url;
        this.showlineNav = (path === '/login' || path === '/register');
      }
    });
    */
    }

  onLogout() {
    //this.showlineNav = true;
    this.auth.clearToken();
    this.auth.authNotifier.next(false);
  }

}
