import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BackendService} from '../../backend.service';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {SubcontractorPaginatorInterface} from '../../../interfaces/subcontractor.interface';

@Injectable()
export class GetAllSubcontractorsResolverService implements Resolve<any>
{

  constructor(private API: BackendService,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<SubcontractorPaginatorInterface>
  {
    return this.API.getAllSubContractors();
  }
}
