import { TestBed, inject } from '@angular/core/testing';

import { GetAllSubcontractorsResolverService } from './get-all-subcontractors-resolver.service';

describe('GetAllSubcontractorsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetAllSubcontractorsResolverService]
    });
  });

  it('should be created', inject([GetAllSubcontractorsResolverService], (service: GetAllSubcontractorsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
