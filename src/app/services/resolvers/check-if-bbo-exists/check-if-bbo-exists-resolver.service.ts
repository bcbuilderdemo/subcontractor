import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BackendService} from '../../backend.service';

@Injectable()
export class CheckIfBboExistsResolverService implements Resolve<any>{

  constructor
  (
    private API: BackendService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    const projectID     = route.params.id;
    const sctID         = route.params.scTradeID;
    return this.API.checkIfBidFormExistsForTrade(projectID, sctID);

  }
}
