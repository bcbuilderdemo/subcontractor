import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {BackendService} from '../../backend.service';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import {Observable} from 'rxjs/Observable';
import {GetProfileInfoInterface} from '../../../interfaces/profile-info.interface';


@Injectable()
export class ProfileResolverService implements Resolve<any>  {



  constructor(
    private API: BackendService,

  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetProfileInfoInterface>
  {
    if( !route.routeConfig ) {
      return this.API.getProfileInfo();
    }
    const path = route.routeConfig.path;
    console.log(path);

    if (path === 'dashboardSampleLayout') {
      return this.API.getProfileInfoForCurrentUser();
    }
    if (path === 'maps/google-map') {
      return this.API.getProfileInfoForCurrentUser();
    }
    if (path === 'dashboard') {
      return this.API.getProfileInfoForCurrentUser();
    }
    else if (path === 'editprofile' || path === 'profile' || path === 'profile-photos') {
      return this.API.getProfileInfo();
    }
    else if (path === 'invoice/project_id/:project_id/subproject_id/:subproject_id' || path === 'project/:id/:subproject_id/invoice') {
      return this.API.getProfileInfo();
    }
    else if (path === 'projects/:id/project-details') {
      return this.API.getProfileInfoForCurrentUser();
    }
    else if (path === 'bid/:bidId/subproject/:subproject_id') {
      return this.API.getProfileInfo();
    }
    // else if (path === 'projects/:id/521/invoice') {
    //   return this.API.getProfileInfo();
    // }
    else if (path === 'profile/:companySlug') {
      // LAZY. Change this so it fetches for current user w/o using params
      if (route.params.companySlug === 'view') {
        return this.API.getProfileInfoForCurrentUser();
      }
      return this.API.getProfileInfoForCompany(route.params.companySlug);
    }

  }


}
