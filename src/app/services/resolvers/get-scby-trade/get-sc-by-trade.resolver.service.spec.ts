import { TestBed, inject } from '@angular/core/testing';

import { GetSCByTradeResolver } from './get-sc-by-trade.resolver.service';

describe('GetScByTrade.ResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetSCByTradeResolver]
    });
  });

  it('should be created', inject([GetSCByTradeResolver], (service: GetSCByTradeResolver) => {
    expect(service).toBeTruthy();
  }));
});
