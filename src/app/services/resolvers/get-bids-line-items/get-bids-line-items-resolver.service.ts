import { Injectable } from '@angular/core';
import { BackendService } from '../../backend.service';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { GetBidsLineItemsInterface } from '../../../interfaces/get-bids-line-items.interface';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class GetBidsLineItemsResolverService implements Resolve<any>
{

    constructor(private API: BackendService,
        private auth: AuthService,
        // private router: Router,
    ) {

    }

    resolve(route: ActivatedRouteSnapshot): Observable<GetBidsLineItemsInterface> {
        return this.API.getAllBidsLineItems(route.params);
    }
}
