import { Injectable } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import {Observable} from 'rxjs/Observable';
import {SessionStorageService} from 'ngx-webstorage';
import {BackendService} from '../../backend.service';

@Injectable()
export class NewOrCreateBidResolverService implements Resolve<any>  {

  projectData: ProjectDataInterface;

  constructor(
    private session: SessionStorageService,
    private API: BackendService,
    private router: Router,
  )
  {

  }
  resolve(route: ActivatedRouteSnapshot): Observable<any>
  {
    console.log("resolve", route.params);
    const routeID = route.params.id;
    let projectID = '';
    this.getProjectData();
    if (!!this.projectData) {
      projectID = this.projectData.id;
      if (projectID == routeID) {
        return Observable.of(this.projectData);
      } else {
        return this.API.getProjectInfo(routeID);
      }
    } else {
      console.log("in else get projectID");
      return this.API.getProjectInfo(projectID);
    }
  }

  setProjectData(data: ProjectDataInterface) {
    console.log("in store");
    this.projectData = data;
    this.session.store('project', this.projectData);
  }

  getProjectData() {
    console.log("get data");
    this.projectData = this.session.retrieve('project');
    return this.projectData;
  }

}
