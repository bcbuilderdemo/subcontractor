import { Injectable } from '@angular/core';
import {BackendService} from '../../backend.service';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {GetProjectsWithSubProjectsInterface} from '../../../interfaces/get-project.interface';

@Injectable()
export class GetProjectsWithSubProjectsDataResolverService implements Resolve<any>  {



  constructor(
    private API: BackendService,

  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetProjectsWithSubProjectsInterface>
  {
    return this.API.getProjectWithSubProjects();
  }

}
