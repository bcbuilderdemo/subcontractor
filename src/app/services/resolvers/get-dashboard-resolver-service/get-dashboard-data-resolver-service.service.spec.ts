import { TestBed, inject } from '@angular/core/testing';

import { GetDashboardDataResolverServiceService } from './get-dashboard-data-resolver-service.service';

describe('GetDashboardDataResolverServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetDashboardDataResolverServiceService]
    });
  });

  it('should be created', inject([GetDashboardDataResolverServiceService], (service: GetDashboardDataResolverServiceService) => {
    expect(service).toBeTruthy();
  }));
});
