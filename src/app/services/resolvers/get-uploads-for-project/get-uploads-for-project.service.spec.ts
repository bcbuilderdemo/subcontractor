import { TestBed, inject } from '@angular/core/testing';

import { GetUploadsForProjectService } from './get-uploads-for-project.service';

describe('GetUploadsForProjectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetUploadsForProjectService]
    });
  });

  it('should be created', inject([GetUploadsForProjectService], (service: GetUploadsForProjectService) => {
    expect(service).toBeTruthy();
  }));
});
