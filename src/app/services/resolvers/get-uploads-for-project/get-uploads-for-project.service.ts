import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BackendService} from '../../backend.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GetUploadsForProjectService implements Resolve<any> {

  constructor(private API: BackendService,
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<any>
  {
    const scTradeID = route.params.scTradeID;
    const projectID = route.params.id;
    return this.API.getSCTsAndBBO(projectID, scTradeID);
  }

}
