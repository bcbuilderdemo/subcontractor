import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BackendService} from '../../backend.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GetScboDataService implements Resolve<any>
{


  constructor
  (
              private API: BackendService,
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<any>
  {
    const projectID   = route.params.id;
    const bidID       = route.params.bidID;
    return this.API.getSCBidOut(projectID, bidID);
  }
}
