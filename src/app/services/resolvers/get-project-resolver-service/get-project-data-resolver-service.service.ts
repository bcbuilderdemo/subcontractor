import { Injectable } from '@angular/core';
import {BackendService} from '../../backend.service';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {GetSubProjectsInterface} from '../../../interfaces/get-project.interface';

@Injectable()
export class GetProjectDataResolverService implements Resolve<any>  {



  constructor(
    private API: BackendService,

  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetSubProjectsInterface>
  {
    return this.API.getSubProjects();
  }

}
