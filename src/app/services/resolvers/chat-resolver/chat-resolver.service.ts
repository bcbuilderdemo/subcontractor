import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BackendService} from '../../backend.service';
import {Observable} from 'rxjs/Observable';
import {GetChatAndUploadsInterface} from '../../../interfaces/chatpaginator.interface';

@Injectable()
export class ChatResolverService implements Resolve<any>
{


  constructor(
    private API: BackendService,
    // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetChatAndUploadsInterface>
  {

    const uuid = route.params.uuid;
    const projectID = route.params.id;

    return this.API.getChat(projectID, uuid);
  }

}
