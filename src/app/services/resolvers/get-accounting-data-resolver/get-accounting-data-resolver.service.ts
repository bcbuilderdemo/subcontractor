import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BackendService} from '../../backend.service';
import {AuthService} from '../../auth/auth.service';
import {Observable} from 'rxjs/Observable';
import {AccountingDataInterface, AccountingInterface} from '../../../interfaces/accounting-data.interface';
import {SessionStorageService} from 'ngx-webstorage';

@Injectable()
export class GetAccountingDataResolverService implements Resolve<AccountingInterface>
{

  constructor(private API: BackendService,
              private auth: AuthService,
              private session: SessionStorageService,
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<AccountingInterface>  {
    const type        = route.params.type;
    console.log("in get accounting resolver", type);

    const projectID   = route.params.id;

    if (this.auth.hasPermission('subcontractor')) {
      if (type === 'all') {
        return this.API.getAllAccounting('default');
      }
      else if (type === 'for-project') {
        return this.API.getSCAccountingForProject(projectID, 'default');

      }
    }
    else if (this.auth.hasPermission('builder')) {
      if (type === 'all') {
        return this.API.getAllAccounting('default');
      }
      else if (type === 'for-project') {
        return this.API.getAccountingForProject(projectID, 'default');
      }
    }
  }

  setAccountingData(data: AccountingDataInterface) {
    this.session.store('accountingData', data);
  }

}
