import { Injectable } from '@angular/core';
import {SubcontractorPaginatorInterface} from '../../../interfaces/subcontractor.interface';
import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BackendService} from '../../backend.service';
import {CreateOrEditOrViewBboDetailsInterface} from '../../../interfaces/create-or-edit-bbo-details.interface';

@Injectable()
export class GetCreateOrEditBbodetailsResolverService implements Resolve<CreateOrEditOrViewBboDetailsInterface> {

  constructor(private API: BackendService,
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<CreateOrEditOrViewBboDetailsInterface>
  {
    const scTradeID = route.params.sctID;
    const projectID = route.params.id;
    const type      = route.params.type;

    console.log("get-or-carte-bbo-resolver", route.params);
    if (type === 'create') {
      return this.API.getProjectInfoForBBO(projectID, scTradeID, type);
    }
    else if (type === 'edit') {
      // in the future change this on the backend so that you retrieve a list of all scs bid sent to
      return this.API.getProjectInfoForBBO(projectID, scTradeID, type);

    }


  }

}
