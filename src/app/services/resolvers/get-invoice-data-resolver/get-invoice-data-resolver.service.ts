import { Injectable } from '@angular/core';
import {BackendService} from '../../backend.service';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {Observable} from 'rxjs/Observable';
import {AccountingAndInvoiceDataInterface} from '../../../interfaces/accountingandinvoice.interface';

@Injectable()
export class GetInvoiceDataResolverService implements Resolve<AccountingAndInvoiceDataInterface>
{

  constructor(private API: BackendService,
              private auth: AuthService,
              // private router: Router,
  )
  {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<AccountingAndInvoiceDataInterface>
  {
      const projectID       = route.params.id;
      const accountingID    = route.params.accountingID;

      if (this.auth.hasPermission('builder')) {
        return this.API.getAccountingAndInvoiceForProject(projectID, accountingID);
      }
      else if (this.auth.hasPermission('subcontractor')) {
        return this.API.getAccountingAndInvoiceForProject(projectID, accountingID);
      }
  }

}
