import { TestBed, inject } from '@angular/core/testing';

import { ViewAllMessagesResolverService } from './view-all-messages-resolver.service';

describe('ViewAllMessagesResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewAllMessagesResolverService]
    });
  });

  it('should be created', inject([ViewAllMessagesResolverService], (service: ViewAllMessagesResolverService) => {
    expect(service).toBeTruthy();
  }));
});
