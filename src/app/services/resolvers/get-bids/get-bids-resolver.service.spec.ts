import { TestBed, inject } from '@angular/core/testing';

import { GetBidsResolverService } from './get-bids-resolver.service';

describe('GetBidsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetBidsResolverService]
    });
  });

  it('should be created', inject([GetBidsResolverService], (service: GetBidsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
