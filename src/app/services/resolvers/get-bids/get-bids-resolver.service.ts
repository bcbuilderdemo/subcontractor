import { Injectable } from '@angular/core';
import { BackendService } from '../../backend.service';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { GetBidsByTradeInterface } from '../../../interfaces/get-bids-by-trade.interface';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class GetBidsResolverService implements Resolve<any>
{

  constructor(private API: BackendService,
    private auth: AuthService,
    // private router: Router,
  ) {

  }

  resolve(route: ActivatedRouteSnapshot): Observable<GetBidsByTradeInterface> {
    const url = route.url[0].path
    let type = route.params.type;
    const projectID = route.params.id
    if (url === 'dashboard') {
      type = 'all';
    }
    if (this.auth.hasPermission('subcontractor')) {
      if (type === 'all') {
        return this.API.getAllBidsByTrade(0, 0, 'all');

      }
      else {
        return this.API.getAllBidsByTrade(route.params.id, 0, 'project');
      }
    }
    else if (this.auth.hasPermission('builder')) {

      if (type === 'all' && projectID) {
        // return this.API.getAllBidsByTrade(0, 0, type);

        return this.API.getAllBidsByTrade(projectID, null, 'project-all');

      }

      if (type === 'all') {
        // return this.API.getAllBidsByTrade(0, 0, type);

        return this.API.getAllBidsByTrade(0, 0, type);

      }
      else {
        return this.API.getAllBidsByTrade(route.params.id, 0, 'default');
      }
    }
  }
}
