import { TestBed, inject } from '@angular/core/testing';

import { GetProfileInfoForDashboardResolverService } from './get-profile-info-for-dashboard-resolver.service';

describe('GetProfileInfoForDashboardResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetProfileInfoForDashboardResolverService]
    });
  });

  it('should be created', inject([GetProfileInfoForDashboardResolverService], (service: GetProfileInfoForDashboardResolverService) => {
    expect(service).toBeTruthy();
  }));
});
