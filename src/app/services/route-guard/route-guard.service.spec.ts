import { TestBed, inject } from '@angular/core/testing';

import { ProjectRouteGuard } from './route-guard.service';

describe('ProjectRouteGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectRouteGuard]
    });
  });

  it('should be created', inject([ProjectRouteGuard], (service: ProjectRouteGuard) => {
    expect(service).toBeTruthy();
  }));
});
