import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ModalComponent } from '../../../templates/modal/modal.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';

@Component({
  selector: 'requet-new-bid-from-subcontractor',
  templateUrl: './request-bid-from-subcontractor.component.html',
  styleUrls: ['./request-bid-from-subcontractor.component.css']
})

export class RequestBidFromSubcontractorComponent implements OnInit {

  @ViewChild('childModal') childModal: ModalComponent;

  project: ProjectDataInterface = null;
  public hbgID: any;
  public formType: any;
  public title: any;
  public description: any;
  public stepName: any;
  public subproject_id: any;
  public buttonTitle;
  public fromRedMarker: any;

  constructor
    (
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
  }
  invoicePage(project_id, subproject_id) {
    this.router.navigate(['/invoice', 'project_id', project_id, 'subproject_id', subproject_id]);
    this.bsModalRef.hide();
  }

  close() {
    this.bsModalRef.hide();
  }
}
