import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestBidFromSubcontractorComponent } from './request-bid-from-subcontractor.component';

describe('RequestBidComponent', () => {
  let component: RequestBidFromSubcontractorComponent;
  let fixture: ComponentFixture<RequestBidFromSubcontractorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestBidFromSubcontractorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestBidFromSubcontractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
