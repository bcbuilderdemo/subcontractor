import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {BBOAndProjectData} from '../../../interfaces/get-bbo-resolver-data.interface';
import {FilesInterface} from '../../../interfaces/files.interface';
import {AuthService} from '../../../services/auth/auth.service';
import {BackendService} from '../../../services/backend.service';
import {SubcontractorInterface} from '../../../interfaces/subcontractor.interface';
import swal from "sweetalert2";
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NewBidInterface} from '../../../interfaces/new-bid.interface';
import {AlertService} from '../../../services/alerts/alert.service';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-review-bid',
  templateUrl: './review-bid.component.html',
  styleUrls: ['./review-bid.component.css']
})
export class ReviewBidComponent implements OnInit {

  project;
  subproject_id;

  constructor
  (
    private route: ActivatedRoute,
    private auth: AuthService,
    private API: BackendService,
    private alerts: AlertService,
    private router: Router,
  )
  {

  }

  ngOnInit() {
    this.project = this.route.snapshot.data.projectData;
    this.subproject_id = this.route.snapshot.params.subproject_id;
    console.log('projects', this.project);
    console.log('subproject', this.subproject_id);
  }

  invoicePage(project_id, subproject_id) {
    this.router.navigate(['/invoice', 'project_id', project_id, 'subproject_id', subproject_id]);
  }

}
