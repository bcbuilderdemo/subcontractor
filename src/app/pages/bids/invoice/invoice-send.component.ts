import { Component, OnInit, TemplateRef, Input, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { GlobalConstants } from '../../../constants/global.constants';
import { AuthService } from '../../../services/auth/auth.service';
import { BackendService } from '../../../services/backend.service';
import { AlertService } from '../../../services/alerts/alert.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { MasksInterface } from '../../../interfaces/masks.interface';
import { PopoverModule, PopoverDirective } from 'ngx-bootstrap';
import swal from "sweetalert2";
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';

@Component({
  selector: 'invoice-send',
  templateUrl: './invoice-send.component.html',
  styleUrls: ['./invoice-send.component.css']
})
export class InvoiceSendComponent implements OnInit {
  @ViewChildren(PopoverDirective) pop: QueryList<PopoverDirective>;

  bsModalRef: BsModalRef;
  bidsLineItems: any;
  bid: any;
  projectID: any;
  subprojectID: any;
  subcontractorProfile: any;
  profileStorageURL: string;
  projectData: any;
  editIndex = null;

  newItem = {
    id: null,
    quantity: 1,
    price: '0',
    details: '',
    bids_id: null
  };
  config = {
    animated: true,
    class: 'modal-md'
  };

  masks: MasksInterface;
  regexPatterns: any;

  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private API: BackendService,
    private alerts: AlertService,
    private router: Router,
    private constants: GlobalConstants,
    private modalService: BsModalService
  ) {
    const snapshot = route.snapshot;
    this.projectID = snapshot.params.project_id;
    this.subprojectID = snapshot.params.subproject_id;
    this.bidsLineItems = snapshot.data.bidInfo.bidLineItems;
    this.subcontractorProfile = snapshot.data.profile.profileInfo;
    if (this.bidsLineItems.length === 0) {
      for (let i = 0; i < 2; i++) {
        let defaultItem = Object.assign({}, this.newItem);
        defaultItem.details = "Please fill out description";
        this.bidsLineItems.push(defaultItem);
      }
    }
    this.bid = snapshot.data.bidInfo.bid || {
      id: null,
      comments: null,
      amount: null
    };
    this.profileStorageURL = `${this.constants.STORAGEURL}/`;
    this.projectData = snapshot.data.projectData;
    this.masks = constants.masks;
    this.regexPatterns = this.constants.regexPatterns;
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.pop.forEach(pop => {
      pop.show();
    })
  }
  addItem(template: TemplateRef<any>) {
    this.editIndex = null;
    this.newItem = {
      id: null,
      quantity: null,
      price: null,
      details: '',
      bids_id: null
    }
    this.bsModalRef = this.modalService.show(template, this.config);
  }

  editItem(index, bidsLineItem, template: TemplateRef<any>) {
    this.editIndex = index;
    this.newItem = Object.assign({}, bidsLineItem);
    this.bsModalRef = this.modalService.show(template, this.config);
  }

  removeItem(index) {
    swal({
      title: 'Are you sure you want to delete this item?',
      type: 'question',
      buttonsStyling: false,
      showConfirmButton: true,
      confirmButtonText: 'Yes   ',
      confirmButtonClass: 'btn btn-outline-success btn-lg mr-2',
      showCancelButton: true,
      cancelButtonText: '  Cancel ',
      cancelButtonClass: 'btn btn-outline-danger btn-lg',
    }).then(() => {
      this.bidsLineItems.splice(index, 1);
    },
      (dismiss) => {
        if (dismiss === 'cancel') {
        }
      });
  }

  submit() {
    if (this.bidsLineItems.length < 2) {
      swal("Oops!", "Please Add two or more items", 'warning');
      return;
    }
    let isEnableSubmit = false;
    this.bidsLineItems.forEach(bidsLineItem => {
      if (!bidsLineItem.details || !bidsLineItem.quantity || !parseInt(bidsLineItem.price)) {
        isEnableSubmit = true;
        return;
      }
    });
    if (isEnableSubmit) return;
    let params = {
      bid: this.bid,
      bids_line_items: this.bidsLineItems,
      project: this.projectData,
      subproject_id: this.subprojectID,
      is_bid_out: 0,
      subcontractorID: this.subcontractorProfile.id,
    }
    this.API.setBidsLineItems(params).subscribe(() => {
      swal("Success", "Invoice successfuly submitted", 'success');
      this.router.navigate(['/dashboard']);
    });
  }

  confirmAddItem() {
    let newItem = this.newItem;
    newItem.price = this.newItem.price.replace(/[$]|,/gi, '');
    if (!this.newItem.quantity || !parseFloat(newItem.price) || !this.newItem.details) {
      return;
    }
    if (this.editIndex !== null) {
      this.bidsLineItems[this.editIndex] = newItem;
    } else {
      newItem.bids_id = this.bid.id;
      this.bidsLineItems.push(newItem);
    }
    this.closeAddItem();
  }

  closeAddItem() {
    this.bsModalRef.hide();
  }

  get total() {
    let total = 0;
    this.bidsLineItems.forEach(bidsLineItem => {
      total += bidsLineItem.quantity * bidsLineItem.price;
    });
    this.bid.amount = total;
    return total;
  }

  get totalNewItem() {
    let price = parseFloat(this.newItem.price.replace(/[$]|,/gi, ''));
    let quantity = this.newItem.quantity;
    return '$' + price * quantity;
  }
}
