import { Component, OnInit } from '@angular/core';
import {BackendService} from '../../../services/backend.service';
import {AlertService} from '../../../services/alerts/alert.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  resetPasswordForm:        FormGroup;
  submitAttempt =           false;

  constructor
  (
    private API: BackendService,
    private alertService: AlertService,
    private router: Router
  )
  {
    this.resetPasswordForm = new FormGroup({
      'email':                         new FormControl('', Validators.compose([Validators.email, Validators.required, Validators.minLength(2), Validators.maxLength(50)])),
    });
  }

  ngOnInit() {
  }

  onSubmit(form: any) {

    if (this.resetPasswordForm.invalid) {
      this.submitAttempt = true;
      return false;
    }
    this.API.forgotPassword(form)
      .subscribe(message => this.alertService.swalSuccess("", message, true, 'Ok', 3000, true, "/login"),
        (error) =>  this.alertService.swalError("Error", "An error has occurred. Please try again", true, 'Ok', null, null, null));
  }

}
