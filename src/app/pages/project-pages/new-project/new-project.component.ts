import {AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {ModalDirective} from "ngx-bootstrap";
import {NgForOf} from "@angular/common";
import {FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {BackendService} from "../../../services/backend.service";
import {NewProjectInterface} from '../../../interfaces/new-project-interface';
import {AlertService} from '../../../services/alerts/alert.service';
import {Router} from '@angular/router';
import {ViewEncapsulation} from '@angular/core';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import {GlobalConstants} from '../../../constants/global.constants';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';
import {MasksInterface} from '../../../interfaces/masks.interface';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class NewProjectComponent implements OnInit, AfterViewInit {

  public config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false
  };


  newProjectForm: FormGroup;

  masks:          MasksInterface;
  regexPatterns:  any;

  submitAttempt         = false;
  projectTypeInvalid    = false;
  zoningInvalid         = false;
  buildingTypeInvalid   = false;

  constructor
  (
    private API: BackendService,
    private modalService: BsModalService,
    public bsModalRef: BsModalRef,
    private alerts: AlertService,
    private router: Router,
    private constants: GlobalConstants,
    private projectService: ProjectResolverService,

  ) {

    this.masks          = constants.masks;
    this.regexPatterns  = this.constants.regexPatterns;

    console.log(this.regexPatterns.postalCode);

    this.newProjectForm = new FormGroup({
      'projectType':                      new FormControl(''),
      'buildingType':                     new FormControl(''),
      'landType':                         new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'address':                          new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'address2':                         new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(100)])),
      'city':                             new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'postalCode':                       new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern(this.regexPatterns.postalCode)])),
      'province':                         new FormControl('BC'),
      'country':                          new FormControl('Canada', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'firstName':                        new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])),
      'lastName':                         new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])),
      'buildValue':                       new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'lotSize':                          new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'description':                      new FormControl(),
      'details':                          new FormControl(),
      'zoning':                           new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'buildingSize':                     new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])),
      'numUnits':                         new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(100)])),
    });
  }

  ngOnInit() {

  }

  ngAfterViewInit() {

  }
  hide() {
    this.bsModalRef.hide();
  }



  onSubmit(input: NewProjectInterface) {

    if (this.newProjectForm.value.buildingType === "") {
      this.buildingTypeInvalid = true;
      this.submitAttempt = true;
      return false;
    }
    if (this.newProjectForm.value.zoning === "") {
      this.zoningInvalid = true;
      this.submitAttempt = true;
      return false;
    }
    if (this.newProjectForm.value.projectType === "") {
      this.projectTypeInvalid = true;
      this.submitAttempt = true;
      return false;
    }
    if (this.newProjectForm.invalid) {
      this.submitAttempt = true;
      return false;
    }
    this.API.newProject(input).subscribe(data => {
      this.bsModalRef.hide();
      this.alerts.swalSuccess(data, '', false, '', 2500, false, '', true);
    });
  }



}
