// zs-trades-listing-component
import { Component, OnInit } from '@angular/core';
import {BackendService} from '../../../services/backend.service';
import {SubcontractorPaginatorInterface} from '../../../interfaces/subcontractor.interface';
import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';
import {ActivatedRoute} from '@angular/router';
import {GetHBGInterface} from '../../../interfaces/get-hbg.interface';
import {GlobalConstants} from '../../../constants/global.constants';
import { SubprojectDataInterface } from '../../../interfaces/subproject.interface';
import { SubprojectResolverService } from '../../../services/resolvers/subproject-resolver/subproject-resolver.service';

@Component({
  selector: 'app-contractors',
  templateUrl: './contractors.component.html',
  styleUrls: ['./contractors.component.css']
})
export class ContractorsComponent implements OnInit {

  scs:                SubcontractorPaginatorInterface;
  hbg:                GetHBGInterface;
  projectData:        ProjectDataInterface;
  storageURL:         string;
  subprojects:        SubprojectDataInterface;

  constructor
  (
    private API: BackendService,
    private projectService: ProjectResolverService,
    private route: ActivatedRoute,
    private constants: GlobalConstants,
  )
  {
    this.hbg = this.route.snapshot.data.hbg;
    this.scs = this.route.snapshot.data.scs;
    this.subprojects = this.route.snapshot.data.subprojects;

    // console.log(this.hbg);
    // console.log(this.scs);
  }

  ngOnInit() {
    this.storageURL   = `${this.constants.STORAGEURL}/`;
    this.projectData  = this.projectService.getProjectData();
    //this.API.getAllSubContractors().subscribe((data) => this.scs = data);
  }

}
