import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendBidComponent } from './send-bid.component';

describe('SendBidComponent', () => {
  let component: SendBidComponent;
  let fixture: ComponentFixture<SendBidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendBidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendBidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
