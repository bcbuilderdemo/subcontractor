import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FilesInterface} from '../../../interfaces/files.interface';
import {GlobalConstants} from '../../../constants/global.constants';
import {BackendService} from '../../../services/backend.service';
import {ProjectResolverService} from '../../../services/resolvers/project-resolver/project-resolver.service';
import {ProjectDataInterface} from '../../../interfaces/get-project.interface';

@Component({
  selector: 'app-view-uploads',
  templateUrl: './view-uploads.component.html',
  styleUrls: ['./view-uploads.component.css']
})
export class ViewUploadsComponent implements OnInit {

  files:                FilesInterface;
  projectData:          ProjectDataInterface;
  storageURL:  string;
  constructor
  (
    private router: Router,
    private route: ActivatedRoute,
    private constants: GlobalConstants,
    private API: BackendService,
    private projectSerivce: ProjectResolverService
  )
  {

  }

  ngOnInit() {
    this.storageURL   = this.constants.STORAGEURL;
    this.files        = this.route.snapshot.data['data'];
    this.projectData  = this.projectSerivce.getProjectData();
  }

  onClickFile(file: FilesInterface) {
    window.open(this.API.getFile(file.fileID));
  }

}
