import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ThemeConstants } from '../../shared/config/theme-constant';
import { NguiMapModule} from '@ngui/map';
import { GoogleMapComponent } from './google-map/google-map.component';

@NgModule({
    imports: [
        NguiMapModule.forRoot({apiUrl: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA80cWOalTZWvHNwZWK9VEVVa3gYzOCKJE'})
    ],
    declarations: [
        GoogleMapComponent
    ],
    providers: [
        ThemeConstants
    ]
})

export class MapsModule { }
