import { GeoCoder } from '@ngui/map/src/services/geo-coder';
import { ProfileResolverService } from '../../../services/resolvers/profile-resolver/profile-resolver.service';
import { Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalComponent } from '../../../templates/modal/modal.component';
import { NewProjectComponent } from '../../project-pages/new-project/new-project.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { BackendService } from '../../../services/backend.service';
import { ProjectDataInterface } from '../../../interfaces/get-project.interface';
import { SessionStorageService } from 'ngx-webstorage';
import { ProjectResolverService } from '../../../services/resolvers/project-resolver/project-resolver.service';
import { GetProjectsWithSubProjectsDataResolverService } from '../../../services/resolvers/get-projects-with-sub-projects/get-projects-with-sub-projects.service';
import { AlertService } from '../../../services/alerts/alert.service';
import { AuthService } from '../../../services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { UserAndCompanyInfo } from '../../../interfaces/user-and-company-info';
import { GlobalConstants } from '../../../constants/global.constants';
import { NotificationsInterface } from '../../../interfaces/notifications.interface';
import { ProfileInfoInterface, ProfileUploadsInterface } from '../../../interfaces/profile-info.interface';
import { GetProjectsWithSubProjectsInterface } from '../../../interfaces/get-project.interface';
import swal from "sweetalert2";
import { forEach } from '@angular/router/src/utils/collection';
import { RequestBidFromSubcontractorComponent } from '../../bids/request-bid-from-subcontractor/request-bid-from-subcontractor.component';

@Component({
  selector: 'app-maps',
  templateUrl: 'google-map.html',
  styleUrls: ['./google-map.component.scss']
})

export class GoogleMapComponent {
  bsModalRef: BsModalRef;
  allSubProjects: any[];
  subProjects: any[];
  allProjects: any[];
  hasNots = false;
  hasProjects = false;
  notKeys;
  maxSize = 5;
  totalItems: number;
  currentPage: number;
  storageURL: string;
  profileInfo: ProfileInfoInterface;
  logo: ProfileUploadsInterface;
  image: ProfileUploadsInterface;
  profileUploads = [] as ProfileUploadsInterface[];
  oldFile: ProfileUploadsInterface;
  // allData: GetProjectsWithSubProjectsInterface;

  clickedSubProjects = null;
  mapConfig = {
    center: { lat: 49.1087238, lng: -122.7794947 },
    zoom: 11
  };

  config = {
    animated: true,
    class: 'modal-md'
  };

  greenIcon = 'http://individual.icons-land.com/IconsPreview/MapMarkers/PNG/Centered/48x48/MapMarker_Marker_Outside_Green.png';
  normalIcon = 'http://individual.icons-land.com/IconsPreview/MapMarkers/PNG/Centered/48x48/MapMarker_Marker_Outside_Red.png';

  // Array that will hold the addresses of each project so we can loop through these in the .html file
  public subProjectMarkers = [];
  public allProjectMarkers = [];
  public emptyProjectMarkers = [];
  clickedProjectMarker = null;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private modalService: BsModalService,
    private API: BackendService,
    private alerts: AlertService,
    private projectService: ProjectResolverService,
    private auth: AuthService,
    private route: ActivatedRoute,
    private constants: GlobalConstants,
    // private allDataService: GetProjectsWithSubProjectsDataResolverService
  ) {
    // Constructor content here

  }

  ngOnInit() {
    const uploads = this.route.snapshot.data.profile.profileUploads;
    this.storageURL = `${this.constants.STORAGEURL}/`;
    this.subProjects = this.route.snapshot.data.data.subprojects;
    this.allSubProjects = this.route.snapshot.data.data.projects;
    this.allProjects = this.route.snapshot.data.data.all_projects;
    this.profileInfo = this.route.snapshot.data.profile.profileInfo;
    // this.allData = this.route.snapshot.data.allData.data;

    Object.values(this.subProjects).forEach((project) => {
      let firstSubproject = project[0];
      this.subProjectMarkers.push({
        id: firstSubproject.project.id,
        address: firstSubproject.project.address,
        city: firstSubproject.project.city,
        province: firstSubproject.project.province,
        postalCode: firstSubproject.project.postalCode,
        subprojects: project,
        fromRedMarker: false,
        companySlug: firstSubproject.project.builder.company_info.companySlug,
        isEmptyProject: false
      })
    });

    Object.values(this.allSubProjects).forEach((project) => {
      let firstSubproject = project[0];
      let isExist = this.subProjectMarkers.find(subprojectMarker => {
        return subprojectMarker.id === firstSubproject.project.id
      });
      if (!isExist) {
        this.allProjectMarkers.push({
          id: firstSubproject.project.id,
          address: firstSubproject.project.address,
          city: firstSubproject.project.city,
          province: firstSubproject.project.province,
          postalCode: firstSubproject.project.postalCode,
          subprojects: project,
          fromRedMarker: true,
          companySlug: firstSubproject.project.builder.company_info.companySlug,
          isEmptyProject: false
        });
      }
    });

    this.emptyProjectMarkers = this.allProjects
      .filter((project) => {
        var found1 = this.subProjectMarkers.find(subprojectMarker => {
          return subprojectMarker.id === project.id
        })
        var found2 = this.allProjectMarkers.find(subprojectMarker => {
          return subprojectMarker.id === project.id
        })
        return found1 === undefined && found2 === undefined
      })
      .map((project) => {
        project.isEmptyProject = true;
        return project;
      });
  }


  public displayLabel({ target: marker }, project) {
    this.clickedProjectMarker = null;
    this.clickedSubProjects = project.subprojects;
    this.clickedSubProjects.fromRedMarker = project.fromRedMarker;

    marker.nguiMapComponent.closeInfoWindow('iwForEmptySubProject', marker);
    marker.nguiMapComponent.openInfoWindow('iw', marker);
  }

  public displayEmptySubprojectLabel({ target: marker }, project) {
    this.clickedSubProjects = null;
    this.clickedProjectMarker = project;

    marker.nguiMapComponent.closeInfoWindow('iw', marker);
    marker.nguiMapComponent.openInfoWindow('iwForEmptySubProject', marker);
  }

  sendBid(subproject) {
    this.onCreateNewBid(subproject);
  }

  onCreateNewBid(subproject) {
    this.bsModalRef = this.modalService.show(RequestBidFromSubcontractorComponent, { class: 'modal-dialog modal-md' });
    this.bsModalRef.content.project = subproject.project;
    this.bsModalRef.content.hbgID = subproject.hbg.id;
    this.bsModalRef.content.subproject_id = subproject.id
    this.bsModalRef.content.stepName = subproject.hbg.stepName
    this.bsModalRef.content.title = "Request a Bid";
    this.bsModalRef.content.description = subproject.description;
    this.bsModalRef.content.buttonTitle = subproject.bid ? "Edit Bid" : "Create Bid";
    this.bsModalRef.content.fromRedMarker = this.clickedSubProjects.fromRedMarker
  }

  displayProjectInformation(template: TemplateRef<any>) {
    this.bsModalRef = this.modalService.show(template, this.config);
  }

  displayEmptySubProjectInformation(template: TemplateRef<any>) {
    let project = this.clickedProjectMarker;
    this.bsModalRef = this.modalService.show(template, this.config);
  }

  close() {
    this.bsModalRef.hide();
  }
}
