#! /bin/bash

echo "Running deploy-firebase.sh"

# echo "installing npm...."
# npm install

echo "Please login to firebase"
firebase login

echo "Init firebase"
firebase init

firebase use --add

#echo "install angular"
#ng install

echo "building angular"
ng build

echo "Deploy Firebase"
firebase deploy

echo "ending script..."

